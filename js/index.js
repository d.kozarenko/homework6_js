"use strict";

let userArray = [1, "DANit", null, 2, "423", []];
function filterBy(array, dataType) {
    let functionArray = [];
    if (!Array.isArray(array)) {
        console.log("Error! Not an array");
        return array;
    } else {
        array.forEach(item => {
            if (typeof item !== dataType) {
                functionArray.push(item);
            }
        });
        return functionArray;
    }
}
console.log(filterBy(userArray, "string"));
console.log(filterBy(userArray, "number"));
console.log(filterBy(userArray, "object"));

// Первым способом мы можем использовать функцию с разными условиями сколько угодно, потому что мы не меняем наш изначальный массив. Второй способ:

// let userArray = [1, "DANit", null, 2, "423", []];
// function filterBy(array, dataType) {
//     let functionArray = [];
//     if(!Array.isArray(array)) {
//         console.log("Error! Not an array");
//         return array;
//     } else {
//         functionArray = array.filter(item => typeof item !== dataType);
//         return functionArray;
//     }
// }
// console.log(filterBy(userArray, "string"));